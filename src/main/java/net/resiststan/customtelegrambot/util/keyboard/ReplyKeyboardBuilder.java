package net.resiststan.customtelegrambot.util.keyboard;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

public class ReplyKeyboardBuilder {
    private List<KeyboardRow> keyboard = new ArrayList<>();

    public static ReplyKeyboardBuilder create() {
        ReplyKeyboardBuilder builder = new ReplyKeyboardBuilder();
        return builder;
    }

    public ReplyKeyboardBuilder row(String... text) {
        KeyboardRow keyboardButtons = new KeyboardRow();

        for (String t : text) {
            keyboardButtons.add(new KeyboardButton(t));
        }

        this.keyboard.add(keyboardButtons);

        return this;
    }

    public ReplyKeyboardMarkup build() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        //вывод клавиатуры всем пользователем
        replyKeyboardMarkup.setSelective(true);
        //подгон размера клывиатуры под экран
        replyKeyboardMarkup.setResizeKeyboard(true);
        //не скрывать клавиатуру после использования
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        //установка клавиатуры
        replyKeyboardMarkup.setKeyboard(this.keyboard);

        return replyKeyboardMarkup;
    }
}
