package net.resiststan.customtelegrambot.util;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;

public interface IUpdateHandler {
    BotApiMethod<? extends Serializable> handle(Update update);
}
