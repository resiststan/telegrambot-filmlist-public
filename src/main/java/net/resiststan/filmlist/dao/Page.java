package net.resiststan.filmlist.dao;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor
@ToString
@Getter
public class Page<T> {
    private final int pageNumber;
    private final int pageSize;
    private final long pageTotalCount;
    private final List<T> data;
}
