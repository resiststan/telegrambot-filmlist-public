package net.resiststan.filmlist.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Iterators;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoException;
import com.mongodb.client.*;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.result.DeleteResult;
import net.resiststan.filmlist.entity.Film;
import net.resiststan.filmlist.util.Converter;
import net.resiststan.filmlist.util.ParameterLoader;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static net.resiststan.filmlist.util.ParameterLoader.*;

public class FilmMongoDB implements IFilmDAO<Film, String> {
    private MongoClient mongoClient;
    protected MongoCollection<Document> col;

    public FilmMongoDB() {
        try {
            mongoClient = MongoClients.create(getConnectString());
            MongoDatabase db = mongoClient.getDatabase(getParam("TB_DB_NAME"));
            col = db.getCollection(getParam("TB_DB_COLLECTION"));

        } catch (MongoException e) {
            System.err.println(e);
            mongoClient.close();
        }
    }

    private String getConnectString() {
        return String.format(
                "mongodb://%s:%s@%s:%s/?authSource=%s",
                getParam("TB_DB_LOGIN"),
                getParam("TB_DB_PASSWORD"),
                getParam("TB_DB_URL"),
                getParam("TB_DB_PORT"),
                getParam("TB_DB_NAME")
        );
    }

    public void close() {
        mongoClient.close();
    }

    @Override
    public Film add(Film film) {
        try {
            col.insertOne(Converter.toDocument(film, Film.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return film;
    }

    @Override
    public Film getUserRandomFilm(Integer userId) {
        AggregateIterable<Document> result = col.aggregate(
                Arrays.asList(
                        Aggregates.match(eq("userId", userId))
                        , Aggregates.match(eq("watch", false))
                        , Aggregates.sample(1)
                )
        );

        Document first = result.first();
        if (first != null) {
            Film film = null;
            try {
                film = (Film) Converter.toPojo(first, Film.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            film.setId(first.getObjectId("_id"));

            return film;
        }

        return null;
    }

    @Override
    public void remove(String filmId) {
        DeleteResult result = col.deleteOne(eq("_id", new ObjectId(filmId)));
    }

    @Override
    public Film markAsWatch(String filmId) {
        Document oneAndUpdate = col.findOneAndUpdate(eq("_id", new ObjectId(filmId)), combine(set("watch", true)));

        Film film = null;
        try {
            film = (Film) Converter.toPojo(oneAndUpdate, Film.class);
            film.setId(oneAndUpdate.getObjectId("_id"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return film;
    }

    @Override
    public Page<Film> paginationUserFilm(Integer userId, Integer pageNumber, Integer pageSize) {
        BasicDBObject whereQuery = new BasicDBObject();
        whereQuery.put("userId", userId);

        long pageTotalCount = getPageCount(col.countDocuments(whereQuery), pageSize);

        MongoCursor<Document> cursor = col.find(whereQuery).skip(pageSize * (pageNumber - 1)).limit(pageSize).iterator();

        List<Film> list = new ArrayList<>();
        cursor.forEachRemaining(document -> {
            try {
                list.add((Film) Converter.toPojo(document, Film.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        cursor.close();
        return new Page<>(pageNumber, pageSize, pageTotalCount, list);
    }

    private long getPageCount(long recordCount, int pageSize) {
        long pageCount = recordCount / pageSize;
        pageCount += (recordCount % pageSize) > 0 ? 1 : 0;
        return pageCount;
    }
}
