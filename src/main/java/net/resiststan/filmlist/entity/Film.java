package net.resiststan.filmlist.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeId;
import lombok.*;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = "id")
public class Film {
    @JsonIgnoreProperties(ignoreUnknown = true)

    @JsonTypeId
    @JsonProperty("_id")
    private ObjectId id;

    @JsonProperty("userId")
    private Integer userId;

    @JsonProperty("storageFilmId")
    private Integer storageFilmId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("nameOriginal")
    private String nameOriginal;

    @JsonProperty("year")
    private Integer year;

    @JsonProperty("country")
    private List<String> country;

    @JsonProperty("time")
    private Integer time;

    @JsonProperty("genre")
    private List<String> genre;

    @JsonProperty("watch")
    private boolean watch;

    @JsonProperty("watchDate")
    private Date watchDate;

    @JsonProperty("description")
    private String description;

    @JsonProperty("url")
    private String url;

    public String tittle() {
        String title = this.name;
        if (this.nameOriginal != null && this.nameOriginal != "") {
            title += " (" + this.nameOriginal + ")";
        }

        return title;
    }
}
