package net.resiststan.filmlist.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class SimpleFilmInfo {

    private String name;
    private String filmId;
    private String originalName;
    private String time;
    private String director;
    private String country;
    private String genre;
    private String year;
    private String queryStr;
    private String url;
}
