package net.resiststan.filmlist.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

import static java.nio.charset.StandardCharsets.UTF_8;

public class ParameterLoader {
    private static Properties params;

    private static final String PROP_PATH = "src/main/resources/app.properties";

    static {
        params = upload(UTF_8, PROP_PATH);
    }

    public static String getParam(String key) {
        return System.getenv(key) == null ? params.getProperty(key) : System.getenv(key);
    }

    public static Properties upload(Charset charsetName, String... path) {
        Properties properties = new Properties();

        for (String s : path) {
            try {
                BufferedReader in =
                        new BufferedReader(new InputStreamReader(new FileInputStream(s), charsetName));

                properties.load(in);
                in.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return properties;
    }
}
