package net.resiststan.filmlist.util;

import java.net.MalformedURLException;
import java.net.URL;

public class Common {
    public static boolean isUrl(String url) {
        //Управлять потоком исполнения программы - не очень,
        //но мы все же сделаем так,
        //пока что...
        try {
            new URL(url);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }
}
