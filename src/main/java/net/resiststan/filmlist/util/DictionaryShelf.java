package net.resiststan.filmlist.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

public abstract class DictionaryShelf {
    private final static Map<String, Properties> shelf = new HashMap<>();

    private final static String DICTIONARY_PATH = "src/main/resources/dictionary/";
    private final static String FILE_NAME_TPL = "\\.dict\\.properties";
    private final static String DEFAULT_KEY = "default";

    static {
        File[] files = findFiles();
        fillDictionary(files);
    }

    private static void fillDictionary(File[] files) {
        for (File f : files) {
            shelf.put(defineKey(f), ParameterLoader.upload(UTF_8, f.getPath()));
        }
    }

    private static File[] findFiles() {
        return new File(DICTIONARY_PATH)
                .listFiles(
                        new FilenameFilter() {
                            private final Pattern p = Pattern.compile(".+" + FILE_NAME_TPL);

                            @Override
                            public boolean accept(File dir, String name) {
                                return p.matcher(name).matches();
                            }
                        }
                );
    }

    private static String defineKey(File f) {
        return f.getName().replaceAll(FILE_NAME_TPL, "");
    }

    public static String get(String key, String dictionaryCode) {
        try {
            return shelf.get(dictionaryCode).getProperty(key, get(key));

        } catch (NullPointerException e) {
            System.err.println(key + " in dictionary " + dictionaryCode + " - dont exist");
        }

        return get(key);
    }

    public static String get(String key) {
        return shelf.get(DEFAULT_KEY).getProperty(key);
    }

    public static List<String> getAllValue(String key) {
        return shelf.entrySet().stream()
                .filter(x -> x.getValue().containsKey(key))
                .map(x -> x.getValue().getProperty(key))
                .collect(Collectors.toList());
    }

    public static boolean containsInAny(String key, String str) {
        return shelf.entrySet().stream()
                .filter(x -> x.getValue().containsKey(key))
                .anyMatch(x -> x.getValue().getProperty(key).equals(str));
    }

    public static Dictionary getDictionary(String dictionaryCode) {
        return new Dictionary(dictionaryCode);
    }

    public static class Dictionary {
        private final String dictionaryCode;

        private Dictionary(String dictionaryCode) {
            this.dictionaryCode = dictionaryCode;
        }

        public String get(String key) {
            return DictionaryShelf.get(key, dictionaryCode);
        }

        public String getDictionaryCode() {
            return dictionaryCode;
        }
    }
}
