package net.resiststan.filmlist.service.messagedata;

import net.resiststan.filmlist.service.method.IMessageData;
import net.resiststan.filmlist.service.AbsServiceKeeper;
import net.resiststan.filmlist.util.DictionaryShelf;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;

import static net.resiststan.filmlist.util.DictionaryShelf.Dictionary;

public interface IMessageDataService extends AbsServiceKeeper.IService {
    IMessageData getSimpleMessage(Dictionary dictionary, String txt);

    IMessageData getRandomFilm(Integer userId, Dictionary dictionary);

    IMessageData addFilm(Message message, Dictionary dictionary);

    IMessageData addFilm(CallbackQuery callbackQuery, Dictionary dictionary);

    IMessageData findFilm(Message message, Dictionary dictionary);

    IMessageData markAsWatchFilm(CallbackQuery callbackQuery, DictionaryShelf.Dictionary dictionary);

    IMessageData removeFilm(CallbackQuery callbackQuery, DictionaryShelf.Dictionary dictionary);

    IMessageData getUserPageFilm(Message message, Dictionary dictionary);

    IMessageData getUserPageFilm(CallbackQuery callbackQuery, Dictionary dictionary);

    IMessageData otherFilm(CallbackQuery callbackQuery, Dictionary dictionary);
}
