package net.resiststan.filmlist.service.cache;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import net.resiststan.filmlist.entity.SimpleFilmInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CacheService implements ICacheService<CacheService.CacheKey, List<SimpleFilmInfo>> {
    private final static Map<CacheKey, List<SimpleFilmInfo>> cache;

    static {
        cache = new HashMap<>();
    }

    @Override
    public List<SimpleFilmInfo> get(CacheKey key) {
        return cache.get(key);
    }

    @Override
    public void put(CacheKey key, List<SimpleFilmInfo> value) {
        cache.put(key, value);
    }

    @Override
    public boolean contain(CacheKey key) {
        return cache.containsKey(key);
    }

    @EqualsAndHashCode(exclude = {"string"})
    @AllArgsConstructor
    @ToString
    @Getter
    public static class CacheKey {
        private final Integer integer;
        private final String string;
    }
}
