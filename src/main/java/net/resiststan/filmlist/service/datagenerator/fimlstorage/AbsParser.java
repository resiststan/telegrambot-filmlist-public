package net.resiststan.filmlist.service.datagenerator.fimlstorage;

import net.resiststan.filmlist.service.datagenerator.DataStorage;
import net.resiststan.filmlist.service.datagenerator.exceptions.StorageAccessException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public abstract class AbsParser<T> implements DataStorage.IDataStorage<T> {
    protected Document rootDocument;

    public AbsParser(String url) throws IOException, StorageAccessException {
        setRootDocument(url);
    }

    private void setRootDocument(String url) throws IOException, StorageAccessException {
        rootDocument = Jsoup.connect(url).get();

        if (invalidDocument()) {
            throw new StorageAccessException();
        }
    }

    public abstract T get();

    public abstract boolean invalidDocument();
}