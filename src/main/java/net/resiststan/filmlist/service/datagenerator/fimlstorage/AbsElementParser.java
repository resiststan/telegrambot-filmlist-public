package net.resiststan.filmlist.service.datagenerator.fimlstorage;

import org.jsoup.nodes.Element;

public abstract class AbsElementParser<T> {
    protected Element rootElement;

    public AbsElementParser(Element rootElement) {
        this.rootElement = rootElement;
    }

    public abstract T get();
}
