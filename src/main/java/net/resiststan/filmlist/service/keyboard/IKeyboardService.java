package net.resiststan.filmlist.service.keyboard;

import net.resiststan.filmlist.service.AbsServiceKeeper;
import net.resiststan.filmlist.util.DictionaryShelf;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

public interface IKeyboardService extends AbsServiceKeeper.IService {

    ReplyKeyboard getKeyboard(IKeyboardType type, DictionaryShelf.Dictionary dictionary, Object param);

    interface IKeyboardType {
    }
}
