package net.resiststan.filmlist.service.method;


import net.resiststan.filmlist.service.AbsServiceKeeper;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface IBotMethodService extends AbsServiceKeeper.IService {
    SendMessage createNewMessage(Message message, @NotNull IMessageData messageData);

    EditMessageText createEditMessageText(Message message, @NotNull IMessageData messageData);
}
