package net.resiststan.filmlist.tbot.execute;

import net.resiststan.filmlist.service.method.IMessageData;
import net.resiststan.filmlist.util.Common;
import net.resiststan.filmlist.util.DictionaryShelf;
import org.jetbrains.annotations.Nullable;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Objects;

public class MessageFactoryImp extends AbsAnswerFactory {

    @Override
    @Nullable
    public SendMessage handle(Update update) {
        IMessageData messageData = getMessageData(update.getMessage());

        if (Objects.nonNull(messageData)) {
            return getBotMethodService().createNewMessage(
                    update.getMessage(),
                    messageData
            );
        }

        return null;
    }

    @Nullable
    private IMessageData getMessageData(Message message) {
        String messageText = message.getText();

        DictionaryShelf.Dictionary dictionary = getDictionary(message.getFrom().getLanguageCode());

        if (isCommand("btn.txt.start", messageText)) {
            return getMessageDataService().getSimpleMessage(dictionary, "msg.start");

        } else if (isCommand("btn.txt.help", messageText)) {
            return getMessageDataService().getSimpleMessage(dictionary, "msg.format.help");

        } else if (isCommand("btn.txt.info", messageText)) {
            return getMessageDataService().getSimpleMessage(dictionary, "msg.info");

        } else if (isCommand("btn.txt.random.film", messageText)) {
            return getMessageDataService().getRandomFilm(message.getFrom().getId(), dictionary);

        } else if (isCommand("btn.txt.all.film", messageText)) {
            return getMessageDataService().getUserPageFilm(message, dictionary);

        } else {
            if (Common.isUrl(messageText)) {
                return getMessageDataService().addFilm(message, dictionary);

            } else {
                return getMessageDataService().findFilm(message, dictionary);

            }
        }
    }

    public static boolean isCommand(String btnKey, String messageText) {
        return DictionaryShelf.containsInAny(btnKey, messageText);
    }
}
